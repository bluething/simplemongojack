package model.hotelinfo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author habib.gunadarma@gmail.com
 *
 */
public class HotelInfoBean {
	private String hotelId;
	private String hotelName;
	private String grade;
	private String hotelType;
	private String latitude;
	private String longitude;
	private String startOpenDate;
	private String refFlg;
	private String countryFlg;
	private String taxRate;
	private String serviceCharge;
	private String currency;
	private String offlineFlg;
	private String netPriceFlg;
	private String provinceCd;
	private String cityCd;
	private String areaCd;
	private String tts;
	private String hotelAddress;
	private String hotelShortDesc;
	private String hotelLongDesc;
	private String yadAdditionalInfo;
	private String postalCode;
	private String currencyPrefix;
	private String seoHotelName;
	private String seoLrgName;
	private List<PictureInfo> pictureInfo = new ArrayList<PictureInfo>();
	private List<AccessInfo> accessInfo = new ArrayList<AccessInfo>();
	private List<HotelFacility> hotelFacility = new ArrayList<HotelFacility>();
	private List<SportFacility> sportFacility = new ArrayList<SportFacility>();
	private List<InternetFacility> internetFacility = new ArrayList<InternetFacility>();
	private List<RoomPlanInfo> roomPlanInfo = new ArrayList<RoomPlanInfo>();
	
	public String getHotelId() {
		return hotelId;
	}
	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}
	public String getHotelName() {
		return hotelName;
	}
	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public String getHotelType() {
		return hotelType;
	}
	public void setHotelType(String hotelType) {
		this.hotelType = hotelType;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getStartOpenDate() {
		return startOpenDate;
	}
	public void setStartOpenDate(String startOpenDate) {
		this.startOpenDate = startOpenDate;
	}
	public String getRefFlg() {
		return refFlg;
	}
	public void setRefFlg(String refFlg) {
		this.refFlg = refFlg;
	}
	public String getCountryFlg() {
		return countryFlg;
	}
	public void setCountryFlg(String countryFlg) {
		this.countryFlg = countryFlg;
	}
	public String getTaxRate() {
		return taxRate;
	}
	public void setTaxRate(String taxRate) {
		this.taxRate = taxRate;
	}
	public String getServiceCharge() {
		return serviceCharge;
	}
	public void setServiceCharge(String serviceCharge) {
		this.serviceCharge = serviceCharge;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getOfflineFlg() {
		return offlineFlg;
	}
	public void setOfflineFlg(String offlineFlg) {
		this.offlineFlg = offlineFlg;
	}
	public String getNetPriceFlg() {
		return netPriceFlg;
	}
	public void setNetPriceFlg(String netPriceFlg) {
		this.netPriceFlg = netPriceFlg;
	}
	public String getProvinceCd() {
		return provinceCd;
	}
	public void setProvinceCd(String provinceCd) {
		this.provinceCd = provinceCd;
	}
	public String getCityCd() {
		return cityCd;
	}
	public void setCityCd(String cityCd) {
		this.cityCd = cityCd;
	}
	public String getAreaCd() {
		return areaCd;
	}
	public void setAreaCd(String areaCd) {
		this.areaCd = areaCd;
	}
	public String getTts() {
		return tts;
	}
	public void setTts(String tts) {
		this.tts = tts;
	}
	public String getHotelAddress() {
		return hotelAddress;
	}
	public void setHotelAddress(String hotelAddress) {
		this.hotelAddress = hotelAddress;
	}
	public String getHotelShortDesc() {
		return hotelShortDesc;
	}
	public void setHotelShortDesc(String hotelShortDesc) {
		this.hotelShortDesc = hotelShortDesc;
	}
	public String getHotelLongDesc() {
		return hotelLongDesc;
	}
	public void setHotelLongDesc(String hotelLongDesc) {
		this.hotelLongDesc = hotelLongDesc;
	}
	public String getYadAdditionalInfo() {
		return yadAdditionalInfo;
	}
	public void setYadAdditionalInfo(String yadAdditionalInfo) {
		this.yadAdditionalInfo = yadAdditionalInfo;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getCurrencyPrefix() {
		return currencyPrefix;
	}
	public void setCurrencyPrefix(String currencyPrefix) {
		this.currencyPrefix = currencyPrefix;
	}
	public String getSeoHotelName() {
		return seoHotelName;
	}
	public void setSeoHotelName(String seoHotelName) {
		this.seoHotelName = seoHotelName;
	}
	public String getSeoLrgName() {
		return seoLrgName;
	}
	public void setSeoLrgName(String seoLrgName) {
		this.seoLrgName = seoLrgName;
	}
	public List<PictureInfo> getPictureInfo() {
		return pictureInfo;
	}
	public void setPictureInfo(List<PictureInfo> pictureInfo) {
		this.pictureInfo = pictureInfo;
	}
	public List<AccessInfo> getAccessInfo() {
		return accessInfo;
	}
	public void setAccessInfo(List<AccessInfo> accessInfo) {
		this.accessInfo = accessInfo;
	}
	public List<HotelFacility> getHotelFacility() {
		return hotelFacility;
	}
	public void setHotelFacility(List<HotelFacility> hotelFacility) {
		this.hotelFacility = hotelFacility;
	}
	public List<SportFacility> getSportFacility() {
		return sportFacility;
	}
	public void setSportFacility(List<SportFacility> sportFacility) {
		this.sportFacility = sportFacility;
	}
	public List<InternetFacility> getInternetFacility() {
		return internetFacility;
	}
	public void setInternetFacility(List<InternetFacility> internetFacility) {
		this.internetFacility = internetFacility;
	}
	public List<RoomPlanInfo> getRoomPlanInfo() {
		return roomPlanInfo;
	}
	public void setRoomPlanInfo(List<RoomPlanInfo> roomPlanInfo) {
		this.roomPlanInfo = roomPlanInfo;
	}
}
