package model.hotelinfo;

/**
 * @author habib.gunadarma@gmail.com
 *
 */
public class PictureInfo {
	private String pictureId;
	private String pictureCaption;
	private String appearanceFlg;
	private String mainCategoryFlg;
	
	public String getPictureId() {
		return pictureId;
	}
	public void setPictureId(String pictureId) {
		this.pictureId = pictureId;
	}
	public String getPictureCaption() {
		return pictureCaption;
	}
	public void setPictureCaption(String pictureCaption) {
		this.pictureCaption = pictureCaption;
	}
	public String getAppearanceFlg() {
		return appearanceFlg;
	}
	public void setAppearanceFlg(String appearanceFlg) {
		this.appearanceFlg = appearanceFlg;
	}
	public String getMainCategoryFlg() {
		return mainCategoryFlg;
	}
	public void setMainCategoryFlg(String mainCategoryFlg) {
		this.mainCategoryFlg = mainCategoryFlg;
	}
}
