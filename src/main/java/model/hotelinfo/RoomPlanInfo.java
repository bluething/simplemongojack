package model.hotelinfo;

import java.util.List;

/**
 * @author habib.gunadarma@gmail.com
 *
 */
public class RoomPlanInfo {
	private String roomTypeCd;
	private String planCd;
	private String roomTypeName;
	private String roomTypeExplain;
	private String planName;
	private String planExplain;
	private String brkfstFlg;
	private String dinnerFlg;
	private String promo1Flg;
	private String promo2Flg;
	private String promo3Flg;
	private String wdNetPrice1;
	private String secretPlanFlg;
	private List<RoomFacility> roomFacility;
	
	public String getRoomTypeCd() {
		return roomTypeCd;
	}
	public void setRoomTypeCd(String roomTypeCd) {
		this.roomTypeCd = roomTypeCd;
	}
	public String getPlanCd() {
		return planCd;
	}
	public void setPlanCd(String planCd) {
		this.planCd = planCd;
	}
	public String getRoomTypeName() {
		return roomTypeName;
	}
	public void setRoomTypeName(String roomTypeName) {
		this.roomTypeName = roomTypeName;
	}
	public String getRoomTypeExplain() {
		return roomTypeExplain;
	}
	public void setRoomTypeExplain(String roomTypeExplain) {
		this.roomTypeExplain = roomTypeExplain;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public String getPlanExplain() {
		return planExplain;
	}
	public void setPlanExplain(String planExplain) {
		this.planExplain = planExplain;
	}
	public String getBrkfstFlg() {
		return brkfstFlg;
	}
	public void setBrkfstFlg(String brkfstFlg) {
		this.brkfstFlg = brkfstFlg;
	}
	public String getDinnerFlg() {
		return dinnerFlg;
	}
	public void setDinnerFlg(String dinnerFlg) {
		this.dinnerFlg = dinnerFlg;
	}
	public String getPromo1Flg() {
		return promo1Flg;
	}
	public void setPromo1Flg(String promo1Flg) {
		this.promo1Flg = promo1Flg;
	}
	public String getPromo2Flg() {
		return promo2Flg;
	}
	public void setPromo2Flg(String promo2Flg) {
		this.promo2Flg = promo2Flg;
	}
	public String getPromo3Flg() {
		return promo3Flg;
	}
	public void setPromo3Flg(String promo3Flg) {
		this.promo3Flg = promo3Flg;
	}
	public String getWdNetPrice1() {
		return wdNetPrice1;
	}
	public void setWdNetPrice1(String wdNetPrice1) {
		this.wdNetPrice1 = wdNetPrice1;
	}
	public String getSecretPlanFlg() {
		return secretPlanFlg;
	}
	public void setSecretPlanFlg(String secretPlanFlg) {
		this.secretPlanFlg = secretPlanFlg;
	}
	public List<RoomFacility> getRoomFacility() {
		return roomFacility;
	}
	public void setRoomFacility(List<RoomFacility> roomFacility) {
		this.roomFacility = roomFacility;
	}
	
	
}
