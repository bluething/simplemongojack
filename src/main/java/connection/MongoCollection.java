package connection;

import java.net.UnknownHostException;
import java.util.Arrays;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.ServerAddress;

/**
 * @author habib.gunadarma@gmail.com
 *
 */
public class MongoCollection {
	private static final String MONGO_DB_HOST = "localhost";
	private static final int MONGO_DB_PORT = 27017;
	private static final String DB_HOTEL_API = "hotelAPI";
	private static final String COLLECTION_HOTEL_INFO = "hotelInfo";
	private static MongoCollection instance = new MongoCollection();
	private static MongoClient mongoClient;
	
	private MongoCollection() {
		MongoClientOptions options = MongoClientOptions.builder()
        .connectionsPerHost(20)
        .build();
		try {
	    mongoClient = new MongoClient(Arrays.asList(
	    	   new ServerAddress(MONGO_DB_HOST, MONGO_DB_PORT)), options);
		} catch (UnknownHostException e) {
	    e.printStackTrace();
    }
	}
	
	public static MongoCollection getInstance() {
		if(instance == null) {
			instance = new MongoCollection();
		}
		return instance;
	}
	
	public DBCollection getHotelInfoCollection() {
		DB db = mongoClient.getDB(DB_HOTEL_API);
		DBCollection dbCollection = db.getCollection(COLLECTION_HOTEL_INFO);
		return dbCollection;
	}
}
