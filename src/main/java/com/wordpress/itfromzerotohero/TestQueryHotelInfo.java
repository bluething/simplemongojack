package com.wordpress.itfromzerotohero;

import java.util.ArrayList;
import java.util.List;

import model.hotelinfo.AccessInfo;
import model.hotelinfo.HotelInfoBean;
import model.hotelinfo.PictureInfo;
import model.hotelinfo.RoomFacility;
import model.hotelinfo.RoomPlanInfo;

import org.mongojack.JacksonDBCollection;

import com.mongodb.DBCollection;
import com.mongodb.WriteResult;

import connection.MongoCollection;

/**
 * @author habib.gunadarma@gmail.com
 *
 */
public class TestQueryHotelInfo {
	public static void main(String[] args) {
		createHotelInfo();
	}
	
	private static void createHotelInfo() {
		DBCollection dbCollection = MongoCollection.getInstance()
				.getHotelInfoCollection();
		JacksonDBCollection<HotelInfoBean, String> coll = JacksonDBCollection.wrap(
				dbCollection, HotelInfoBean.class, String.class);
		HotelInfoBean bean = getHotelInfoBean();
		org.mongojack.WriteResult<HotelInfoBean, String> result = coll.insert(bean);
	}
	
	private static HotelInfoBean getHotelInfoBean() {
		HotelInfoBean bean = new HotelInfoBean();
		bean.setHotelId("98650");
		bean.setHotelName("Dummy Hotel");
		
		List<PictureInfo> pictureInfo = new ArrayList<PictureInfo>();
		for(int i=0; i<2; i++) {
			PictureInfo info = new PictureInfo();
			info.setPictureId(String.valueOf(i));
			info.setPictureCaption("picture caption");
			info.setAppearanceFlg(String.valueOf(i));
			info.setMainCategoryFlg(String.valueOf(i));
			pictureInfo.add(info);
		}
		bean.setPictureInfo(pictureInfo);
		
		List<AccessInfo> accessInfo = new ArrayList<AccessInfo>();
		for(int i=0; i<2; i++) {
			AccessInfo info = new AccessInfo();
			info.setDescription("description accessInfo");
			info.setName("name accessInfo");
			info.setValue(String.valueOf(i));
			accessInfo.add(info);
		}
		bean.setAccessInfo(accessInfo);
		
		List<RoomPlanInfo> roomPlanInfoList = new ArrayList<RoomPlanInfo>();
		RoomPlanInfo roomPlanInfo = new RoomPlanInfo();
		roomPlanInfo.setRoomTypeCd("ert000");
		
		List<RoomFacility> roomFacility = new ArrayList<RoomFacility>();
		for(int i=0; i<2; i++) {
			RoomFacility facility = new RoomFacility();
			facility.setDescription("description facility");
			facility.setName("name facility");
			facility.setValue(String.valueOf(i));
			roomFacility.add(facility);
		}
		roomPlanInfo.setRoomFacility(roomFacility);
		roomPlanInfoList.add(roomPlanInfo);
		bean.setRoomPlanInfo(roomPlanInfoList);
		
		return bean;
	}
}
